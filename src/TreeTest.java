import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;

public class TreeTest implements Runnable {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new TreeTest());
	}

	private static class DriveComboboxModel extends DefaultComboBoxModel<File> {
		private static final long serialVersionUID = -3281943090137396181L;

		public DriveComboboxModel() {
			File[] roots = File.listRoots();
			for (File file : roots) {
				this.addElement(file);
			}
		}
	}

	private static class FileTreeFrame extends JFrame implements ItemListener {
		private static final long serialVersionUID = 1L;
		private JComboBox<File> driveComboBox;
		private FileTreeModel model;
		private JTree fileTree;

		public FileTreeFrame() {
			super("JTree example");
			setDefaultCloseOperation(EXIT_ON_CLOSE);

			DriveComboboxModel driveModel = new DriveComboboxModel();
			driveComboBox = new JComboBox<File>(driveModel);
			driveComboBox.addItemListener(this);
			add(driveComboBox, BorderLayout.NORTH);

			model = new FileTreeModel(new FileWrap(driveModel.getElementAt(0)));
			fileTree = new JTree(model);
			fileTree.setCellRenderer(new FileTreeRenderer());
			JScrollPane scrollPane = new JScrollPane(fileTree);

			ToolTipManager.sharedInstance().registerComponent(fileTree);

			add(scrollPane, BorderLayout.CENTER);

			pack();
		}

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				model.setRoot((File) e.getItem());
			}
		}
	}

	@Override
	public void run() {
		FileTreeFrame frame = new FileTreeFrame();
		frame.setVisible(true);
	}
}
