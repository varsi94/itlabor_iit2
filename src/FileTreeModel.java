import java.io.File;
import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class FileTreeModel implements TreeModel {
	private FileWrap root;
	private List<TreeModelListener> listeners;

	private Comparator<File> fileComparator = new Comparator<File>() {
		private RuleBasedCollator collator = (RuleBasedCollator) Collator.getInstance(Locale.forLanguageTag("hu")); 

		@Override
		public int compare(File a, File b) {
			return collator.compare(a.getName().toLowerCase(), b.getName().toLowerCase());
		}
	};

	public FileTreeModel(FileWrap defaultRoot) {
		root = defaultRoot;
		listeners = new ArrayList<TreeModelListener>();
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		listeners.add(l);
	}

	@Override
	public Object getChild(Object parent, int index) {
		FileWrap wrap = (FileWrap) parent;
		if (wrap.value.isDirectory()) {
			File[] f = wrap.value.listFiles();
			Arrays.sort(f, fileComparator);
			return new FileWrap(f[index]);
		} else {
			return null;
		}
	}

	@Override
	public int getChildCount(Object parent) {
		FileWrap wrap = (FileWrap) parent;
		if (wrap.value.isFile()) {
			return 0;
		} else {
			if (wrap.value.listFiles() != null) {
				return wrap.value.listFiles().length;
			} else {
				// Előfordulhat, hogy nincs jogunk a könyvtár listázásához
				// ilyenkor 0-t adunk vissza, mint a gyerekek száma.
				return 0;
			}
		}
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		FileWrap parentWrap = (FileWrap) parent;
		FileWrap childWrap = (FileWrap) child;
		if (parentWrap.value.isDirectory()) {
			File[] f = parentWrap.value.listFiles();
			Arrays.sort(f, fileComparator);
			return Arrays.binarySearch(f, childWrap.value, fileComparator);
		} else {
			return -1;
		}
	}

	@Override
	public Object getRoot() {
		return root;
	}

	@Override
	public boolean isLeaf(Object node) {
		FileWrap wrap = (FileWrap) node;
		return wrap.value.isFile();
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		listeners.remove(l);
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {

	}

	public void setRoot(File root) {
		FileWrap oldRoot = this.root;
		this.root.value = root;
		for (TreeModelListener treeModelListener : listeners) {
			TreePath path = new TreePath(oldRoot);
			treeModelListener.treeStructureChanged(new TreeModelEvent(this,
					path));
		}
	}
}
