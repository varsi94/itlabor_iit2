import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

public class FileTreeRenderer extends DefaultTreeCellRenderer {
	private static final long serialVersionUID = 5020848155645589227L;

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		JComponent comp = (JComponent) super.getTreeCellRendererComponent(tree,
				value, selected, expanded, leaf, row, hasFocus);
		FileWrap wrap = (FileWrap) value;
		if (wrap.value.isDirectory()) {
			comp.setToolTipText("Könyvtár");
		} else {
			long size = wrap.value.length();
			double sizeInMB = size / 1024.0 / 1024.0;
			comp.setToolTipText(String.format("%.2f MB", sizeInMB));
		}
		return comp;
	}

}
