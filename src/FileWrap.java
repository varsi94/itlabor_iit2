import java.io.File;

public class FileWrap {
	public File value;

	public FileWrap(File file) {
		value = file;
	}

	@Override
	public String toString() {
		return value.getName();
	}
}
